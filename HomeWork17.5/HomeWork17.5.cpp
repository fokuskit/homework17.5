#include <iostream>
#include <math.h>
#include <cmath>


class vector
{
private:
    double x;
    double y;
    double z;
    double X;

public:
    vector() : x(0), y(0), z(0) , X(0)
    {}
    vector(double _x, double _y, double _z, double _X) : x(_x), y(_y), z(_z), X(_X)
    {}
    int length()
    {

       X = (x * x + y * y + z * z);
       return(static_cast<int>(sqrt(X)));
    }
};


class example
{
private:
    int a;

public:
    int GET()
    {
        return a;
    }
    void SET(int setA)
    {
        a = setA;
    }


};

int main()
{
    example temp;
    temp.SET(10);
    std::cout << temp.GET() << "\n";

    vector C(10, 20, 30, 0);
    std::cout << C.length();
}